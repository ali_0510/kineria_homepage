$( document ).ready(function() {
   $(".button-collapse").sideNav();
   $('.slider').slider({
   		full_width: true,
   		height:400
   	});
   $('.parallax').parallax();
   $('.gallery-view-home').slick({
    	  dots: false,
		  infinite: false,
		  speed: 300,
		  slidesToShow: 4,
		  slidesToScroll: 4,
		  responsive: [
		    {
		      breakpoint: 1160,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 3,
		        infinite: true,
		        dots: false
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2,
		        dots: false
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2,
		        dots: false,
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		    // You can unslick at a given breakpoint now by adding:
		    // settings: "unslick"
		    // instead of a settings object
		  ]
   });
   $('#testimonial').slick({
	  dots: true,
	  infinite: true,
	  arrows: false,
	  speed: 500,
	  fade: true,
	  cssEase: 'linear'
	});


   $('.watchlist-group').slick({
	  dots: false,
	  infinite: false,
	  speed: 300,
	  slidesToShow: 6,
	  slidesToScroll: 6,
	  responsive: [
	    {
	      breakpoint: 1025,
	      settings: {
	        slidesToShow: 4,
	        slidesToScroll: 4,
	        infinite: true,
	        dots: true
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});

   var removeClass = true;
	$(".subscriber-form").click(function () {
	    $(".subscriber-form").toggleClass('active-form');
	    removeClass = false;
	});

	$(".subscriber-form").click(function() {
	    removeClass = false;
	});

	$("html").click(function () {
	    if (removeClass) {
	        $(".subscriber-form").removeClass('active-form');
	    }
	    removeClass = true;
	});

   $('#back-to-top').on('click', function (e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: 0
        }, 700);
    });

   $('.modal-trigger').leanModal({
      dismissible: true, // Modal can be dismissed by clicking outside of the modal
      opacity: .5, // Opacity of modal background
      in_duration: 300, // Transition in duration
      out_duration: 200, // Transition out duration
     }
  );

  $(".forgot-account").click(function(){
  		$("#login_form").hide();
  		$("#forgot_form").show();
  });
  $(".backto-form").click(function(){
  		$("#login_form").show();
  		$("#forgot_form").hide();
  }); 
 $('textarea').focus(function() { 

 	$(".reply_comment").css("border-color", "#26A69A")
 	$('head').append("<style>#comments:before { border-bottom-color:#26A69A; }</style>");
 	

  })
 	.blur(function() { 
 		if ($(this)[0].value == '') { 
 			$(".reply_comment").css("border-color", "#9e9e9e")
 			$('head').append("<style>#comments:before { border-bottom-color:#9e9e9e; }</style>");
 			
 		} 
  });

$(function(){
	$('#content').scrollPagination({
		'contentPage': 'infinite-files/koleksi.html', // the url you are fetching the results
		'contentData': {}, // these are the variables you can pass to the request, for example: children().size() to know which page you are
		'scrollTarget': $(window), // who gonna scroll? in this example, the full window
		'heightOffset': 10, // it gonna request when scroll is 10 pixels before the page ends
		'appendCallback': false,
		'pixelsFromNavToBottom': 50,
		'beforeLoad': function(){ // before load function, you can display a preloader div
			$('#loading').fadeIn();	
		},
		'afterLoad': function(elementsLoaded){ // after loading content, you can use this function to animate your new elements
			 $('#loading').fadeOut();
			 var i = 0;
			 $(elementsLoaded).fadeInWithDelay();
			 if ($('#content').children().size() > 100){ // if more than 100 results already loaded, then stop pagination (only for testing)
			 	// $('#nomoreresults').fadeIn();
				$('#content').stopScrollPagination();
			 }
		}
	});
	
	// code for fade in element by element
	$.fn.fadeInWithDelay = function(){
		var delay = 0;
		return this.each(function(){
			$(this).delay(delay).animate({opacity:1}, 200);
			delay += 300;
		});
	};
		   
});

$('.tooltipped').tooltip({delay: 50});

$(".add-comment").click(function(){
	$("#reply_comment").slideToggle();
});

$(".search-trigger").click(function(){
	$("#search-field-form").slideToggle();

});

$(".close-trigger-search").click(function(){
	$("#search-field-form").fadeOut();

});
$('.dropdown-element').dropdown({
  inDuration: 300,
  outDuration: 225,
  constrain_width: false, // Does not change width of dropdown to that of the activator
  hover: false, // Activate on hover
  gutter: 0, // Spacing from edge
  belowOrigin: true, // Displays dropdown below the button
  alignment: 'left' // Displays dropdown with edge aligned to the left of button
}
);
$('.dropdown-smenu').dropdown({
  inDuration: 300,
  outDuration: 225,
  constrain_width: false, // Does not change width of dropdown to that of the activator
  hover: false, // Activate on hover
  gutter: 0, // Spacing from edge
  belowOrigin: true, // Displays dropdown below the button
  alignment: 'left' // Displays dropdown with edge aligned to the left of button
})
$(".dropdown-element").click(function(){
	$("i.indicator").addClass("active");
});
$('.dropdown-element').click(function() {
     $("i.indicator", this).toggleClass("fa-angle-up fa-angle-down");
});
  
$('.dropdown-trigger').click(function() { 	
	$("#menu-user").slideToggle();
});




});

// $(window).scroll(function() {

//     if ($(this).scrollTop()>400)
//      {
//         $('footer').fadeIn();
//      }
//     else
//      {
//       $('footer').fadeOut();
//      }
//  });

 